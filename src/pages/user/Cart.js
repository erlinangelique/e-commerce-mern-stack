import { useEffect, useState } from 'react';
import { Container, Table, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function UserOrder() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/user-orders`, 
    // fetch(`http://localhost:4000/orders/user-orders`, 
    {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          const modifiedData = data.map((order) => ({
            user: order.user,
            products: order.products.map((product) => ({
              productId: product.productId,
              name: product.name,
              description: product.description,
              price: product.price,
              quantity: product.quantity,
              subTotal: product.subTotal,
            })),
            totalAmount: order.totalAmount,
          }));
          setOrders(modifiedData);
        } else if (typeof data === 'object' && data !== null) {
          const modifiedData = {
            user: data.user,
            products: data.products.map((product) => ({
              productId: product.productId,
              name: product.name,
              description: product.description,
              price: product.price,
              quantity: product.quantity,
              subTotal: product.subTotal,
            })),
            totalAmount: data.totalAmount,
          };
          setOrders([modifiedData]);
        } else {
          console.error('Data is not an array or an object:', data);
        }
      })
      .catch((error) => console.error(error));
  }, []);

  const handleQuantityChange = (productId, newQuantity) => {
    const modifiedOrders = orders.map((order) => {
      const modifiedProducts = order.products.map((product) => {
        if (product.productId === productId) {
          return {
            ...product,
            quantity: newQuantity,
            subTotal: newQuantity * product.price,
          };
        }
        return product;
      });
      const modifiedTotalAmount = modifiedProducts.reduce(
        (total, product) => total + product.subTotal,
        0
      );
      return {
        ...order,
        products: modifiedProducts,
        totalAmount: modifiedTotalAmount,
      };
    });
    setOrders(modifiedOrders);
  };

  return (
    <Container className="mt-5 justify-content-center">
      {orders.map((order) => (
        <div key={order.user._id}>
          <h3>
            {order.user.firstName} {order.user.lastName}
          </h3>
          <Table striped bordered className="my-table">
            <thead>
              <tr>
                <th>Product Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
              {order.products.map((product) => (
                <tr key={product.productId}>
                  <td>{product.name}</td>
                  <td>&#x20B1; {product.price}</td>
                  <td>
                    <Form.Control
                      type="number"
                      value={product.quantity}
                      onChange={(event) =>
                        handleQuantityChange(
                          product.productId,
                          parseInt(event.target.value)
                        )
                      }
                    />
                  </td>
                  <td>&#x20B1; {product.subTotal}</td>
                </tr>
              ))}
              <tr>
                <td colSpan="3">Total</td>
                <td>&#x20B1; {order.totalAmount}</td>
              </tr>
            </tbody>
          </Table>
                 {/* <Link
          to={{
            pathname: '/checkout',
            state: { totalAmount: order.totalAmount },
          }}
          className="checkoutBtn btn btn-primary"
        >
          Checkout
        </Link>*/}
      </div>
    ))}
  </Container>
);
}
             
