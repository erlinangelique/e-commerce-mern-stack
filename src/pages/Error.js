import {Container, Row, Col, Card} from "react-bootstrap";
import {Link} from "react-router-dom"
import React from 'react';

export default function Error() {
	return (
		<Container className="justify-content-center d-flex align-items-center">
			<Row>
				<Col className="mx-auto">
					<Card id="card-error" className= "mt-5 border-0">
					    <Card.Body className="text-center">
					        <Card.Title id="error-title">PAGE NOT FOUND</Card.Title>
					        <Card.Text id="error-text">We're sorry, but the page you requested could not be found.</Card.Text>
					        <Link id="error-link-shop" className="btn-shop mt-4 mx-2 btn btn-dark" to="/products">CONTINUE SHOPPING</Link>
					        <Link id="error-link-home" className="btn-home mt-4 btn btn-dark" to="/">GO TO HOME PAGE</Link>
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
