import { useEffect, useState } from 'react';
import { Card, Table, Accordion } from 'react-bootstrap';

export default function GetAllOrders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/`,
    // fetch(`http://localhost:4000/orders/`,
     {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => setOrders(data))
      .catch((error) => console.error(error));
  }, []);

  return (
    <Accordion className="mt-5">
      {orders.map((order) => (
        <Accordion.Item eventKey={order.user._id} key={order.user._id}>
          <Accordion.Header>
            {order.user.firstName} {order.user.lastName}
          </Accordion.Header>
          <Accordion.Body>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody>
                {order.products.map((product) => (
                  <tr key={product._id}>
                    <td>{product.name}</td>
                    <td>&#x20B1; {product.price}</td>
                    <td>{product.quantity}</td>
                    <td>&#x20B1; {product.subTotal}</td>
                  </tr>
                ))}
                <tr>
                  <td colSpan={3} style={{ textAlign: 'right' }}>Total:</td>
                  <td>&#x20B1; {order.totalAmount}</td>
                </tr>
              </tbody>
            </Table>
          </Accordion.Body>
        </Accordion.Item>
      ))}
    </Accordion>
  );
}
