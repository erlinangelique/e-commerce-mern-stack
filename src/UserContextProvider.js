import React, { createContext, useState } from 'react';

export const UserContext = createContext();

export function UserContextProvider({ children }) {
  const [order, setOrder] = useState(null);

  const unsetOrder = () => {
    setOrder(null);
  };

  return (
    <UserContext.Provider value={{ order, setOrder, unsetOrder }}>
      {children}
    </UserContext.Provider>
  );
}

export default UserContextProvider;
