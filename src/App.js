import './App.css';

// built-in react modules imports
import { useState, useEffect } from 'react';

// downloaded package modules imports
import { Container } from 'react-bootstrap';
import { Route, Routes, BrowserRouter as Router } from 'react-router-dom';

// (user-defined) components imports (alphabetical or according file structure)
// ========== USER ========== //
import Login from './pages/user/Login';
import Register from './pages/user/Register';
import Logout from './pages/user/Logout';
import Cart from './pages/user/Cart';
import Checkout from './pages/user/Checkout';

// ========== PRODUCT ========== //
import AddNew from './pages/product/AddNew';
import GetActive from './pages/product/GetActive';
import Dashboard from './pages/product/Dashboard';
import GetSpecific from './pages/product/GetSpecific';
import Update from './pages/product/Update';

// ========== ORDER ========== //
import GetUserOrders from './pages/order/GetUserOrders';

// ========== OTHERS ========== //
import AppNavbar from './components/AppNavbar';
import Error from './pages/Error';
import Home from './pages/Home';

import { UserProvider } from './UserContext'




function App() {

  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [ user, setUser ] = useState({
      id: null,
      isAdmin: null,
      firstName: ""
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    const token = localStorage.getItem('token');

    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`,
      // fetch(`http://localhost:4000/users/details`,
      {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          firstName: data.firstName
        })
      });
    }
  }, [])


  return (

  //Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop.
  <UserProvider value={{ user, setUser, unsetUser }}>
    <Router>
      <AppNavbar/>
          <Container>     
            <Routes>
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />}/>
              <Route path="/products" element={<GetActive />}/>
              <Route path="/orders" element={<GetUserOrders />}/>
              <Route path="/users/orders" element={<Cart />}/>
              <Route path="/orders/checkout" element={<Checkout />}/>
              <Route path="/products/create" element={<AddNew />} />   
              <Route path="/products/:productId" element={<GetSpecific />} />
              <Route path="/products/:productId/update" element={<Update />} />   
              <Route path="/register" element={<Register />} />
              <Route path="*" element={<Error />} />
            </Routes>
        </Container>
    </Router>
  </UserProvider>
  );
}

export default App;
